const routes = [
  {
    path: '/',
    redirect: '/page1',
  },
  {
    path: '/page1',
    component: () => import('@/views/page1.vue'), //路由懒加载
  },
];

export default routes;
