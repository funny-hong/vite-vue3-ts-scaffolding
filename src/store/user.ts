import { defineStore } from 'pinia';
export const useUserStore = defineStore('user', {
  state: () => ({}),
  getters: {},
  actions: {},

  // 开始数据持久化
  persist: {
    key: 'storekey', // 修改存储的键名，默认为当前 Store 的 id
    storage: window.sessionStorage, // 存储位置修改为 sessionStorage
  },
});
